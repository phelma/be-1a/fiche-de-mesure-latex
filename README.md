# Licence

To be determined...

# Authors

Original contents: Matthieu Guerquin-Kern, Soline Beitone (some of the example contents)

# Contents

This folder contains the template and example LaTeX sources for labwork reports.

# Origin

These sources are hosted on a server using hosting [GITLAB](https://gitlab.com).

You can see the activity of the project at
<https://gricad-gitlab.univ-grenoble-alpes.fr/phelma/be-1a/fiche-de-mesure-latex>.
There, among other things, you can download the latest version of the project files.
You can also access the project through GIT.
```bash
$ git clone git@gricad-gitlab.univ-grenoble-alpes.fr:phelma/be-1a/fiche-de-mesure-latex.git
```

# Requirements

The building setup has been only tested under a GNU/LINUX environment (Ubuntu, specifically).
Although, the building process might be also set on Windows and MacOs environments, the authors will
only support the use of GNU/LINUX. Windows/MacOs users can install GNU/Linux with
minimum modification of their main working environment using either
1. a LiveUSB installation (see https://en.wikipedia.org/wiki/List_of_tools_to_create_Live_USB_systems), or
2. a virtual machine (see virtualbox for instance).

An installation of the TexLive suite is required. The complete installation is not necessary but the
following packages are known to be necessary:
* texlive-latex-recommended
* texlive-fonts-recommended
* texlive-fonts-extra
* texlive-lang-french
* texlive-science
* texlive-pictures
* texlive-latex-extra

# Building

You can compile the `.tex` files using pdfLaTeX, either with your favorite editor or in command line
```bash
$ pdflatex file_to_be_compiled.tex
```
