\documentclass[a4paper,french,twocolumn]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage[colorlinks=true]{hyperref}
\usepackage[twocolumn,a4paper,
 left=18mm,right=18mm,
 top=18mm,bottom=18mm]{geometry}
\usepackage[protrusion=true,expansion=true,final,babel]{microtype}
\usepackage{libertine}
\usepackage{booktabs}
\usepackage[draft]{graphicx}% 'draft' n'inclut pas véritablement les images
\usepackage{pgfplotstable}
\usepackage{siunitx}
\sisetup{scientific-notation = engineering,
group-separator = \text{~},
exponent-product = \cdot,
output-decimal-marker = {,},
inter-unit-separator=\ensuremath{{} \cdot {}},
load=accepted,
round-mode          = places, % Rounds numbers
round-precision     = 2, % to 2 places
separate-uncertainty = true
}

\def\uj{\mathrm{j}}

\title{Fabrication d'un récepteur radio AM\\[0.2em]
\Large --- Filtre sélectif ---}
\author{\textsc{Doe} Jeanette}
\date{1\ier\ janvier 2021}% éventuellement remplacer par le contenu par \today

\begin{document}
% le contenu de ce document est inspiré d'un travail réalisé par Soline Beitone 
% en 2020.

\maketitle

\begin{abstract}
Ce document témoigne d'un travail effectué en 2021 dans le cadre des bureaux 
d'étude (BE) d'électronique à \href{https://phelma.grenoble-inp.fr/}{Grenoble 
INP -- Phelma}, en 1\iere\ année de formation initiale sous statut étudiant, 
filière PMP.

Ici, c'est la partie \og filtre sélectif\fg\ du projet qui nous intéresse. 
La figure~\ref{fig:montage_global} illustre son rôle dans l'ensemble du 
dispositif à mettre en \oe uvre.
\begin{figure}[htbp]
\caption{Montage global, \textit{partie filtre sélectif HF.}}
\label{fig:montage_global}
\centering
\includegraphics[width=0.95\linewidth,height=6.5cm]{etape filtre sélectif.png}
\end{figure}

%\begin{center}
%\begin{tabular}{ll}
%\toprule
%Début du projet  & 21/11/2020              \\
%Validation initiale & \dotfill \\% \hline \hline
%Validation finale & \dotfill \\% \hline
%\bottomrule
%\end{tabular}
%\end{center}

\end{abstract}

\tableofcontents

\section{Introduction}

Le but de cette partie est de concevoir un filtre sélectif en entrée qui doit 
ne laisser passer en sortie que la bande de fréquence d'intérêt, autour de  
\SI{162}{\kilo\hertz}. 
L'enjeu est donc de mettre en \oe uvre un filtre passe bande qui soit à la fois 
assez sélectif pour sélectionner uniquement la fréquence d'intérêt mais dont la 
largueur de la bande passante soit suffisamment grande pour conserver
l'ensemble du spectre de la fonction modulante et donc de l'information à 
restaurer.

Le cahier des charges de ce filtre est:
\begin{itemize}
\item type passe-bande,
\item fréquence centrale de \SI{162}{\kilo\hertz} et
\item bande-passante de \SI{16}{\kilo\hertz}.
\end{itemize}

\paragraph{Remarque} Nous allons utiliser des composants de la série E12 et 
simuler nos résultats sur LTSpice.

\section{Dimensionnement}

Pour cet étage d'entrée traitant un signal de relativement haute fréquence, il 
est souhaitable de mettre en \oe uvre un filtre passif. Nous choisissons une 
structure $RLC $ série qui est passive, de faible complexité et qui permet de 
réaliser un filtre passe-bande sélectif. L'enjeu de cette partie est de 
choisir les valeurs de ses composants pour respecter le cahier des charges.

\subsection{Bande passante du montage}

On veut sélectionner la bande fréquences de largueur \SI{16}{\kilo\hertz} 
autour de la fréquence de porteuse $f_p=\SI{162}{\kilo\hertz}$.
Ainsi, on cherche une bande passante s'étendant de 
\SIrange{154}{170}{\kilo\hertz}.

\subsection{Facteur de qualité}
En étudiant la structure de filtre $RLC$ série, avec une sortie sous forme de 
tension aux bornes de la résistance, on obtient une fonction de transfert de 
passe-bande du second ordre, sous forme canonique
\[H(\uj\omega)= 
\frac{H_{0}}{1+\uj Q(\frac{\omega}{\omega_{0}} - \frac{\omega_{0}}{\omega})},\]
avec les paramètres $H_{0} = 1$ (gain maximal), $Q = \sqrt{L/C}/R$ (facteur de 
qualité) et $\omega_{0} = 1/\sqrt{LC}$ (pulsation dite \og propre\fg, \og 
centrale\fg\ ou \og de résonance\fg).

\subsection{Choix des paramètres du modèle $RLC$ série}

C'est sur l'inductance que les contraintes matérielles sont les plus fortes. On 
impose donc $L = \SI{10}{\milli\henry}$.

D'une part, la fréquence de porteuse fixe la pulsation centrale : 
$\omega_0=2\pi f_p\approx \SI{1.02e6}{\radian\per\second}$. On doit donc choisir
$C=1/(L\omega_0^2)\approx \SI{96.12}{\pico\farad}$.

D'autre part, on veut obtenir une bande passante $\Delta f = 
\SI{16}{\kilo\hertz}$. Ceci impose un facteur de qualité 
$Q=\omega_0/(2\pi\Delta f)\approx \num{10.125}$. Alors, on trouve 
$R=\sqrt{L/C}/Q\approx\SI{1.007}{\kilo\ohm}$.

\paragraph{Choix final} En choisissant des composants dans 
la série E12, on a conçu un circuit $RLC$ série avec $R=\SI{1}{\kilo\ohm}$, 
$L=\SI{10}{\milli\henry}$ et $C=\SI{100}{\pico\farad}$. Avec ces valeurs, on 
obtient un facteur de qualité théorique $Q = 10$.

\section{Simulation (validation \emph{in silico})}
Dans cette partie, nous rendons compte de simulations visant à valider le 
dimensionnement du filtre.

\subsection{Mise en \oe uvre et résultats}
\begin{figure}[htbp]
\caption{Circuit électrique utilisé en simulation.}
\label{fig:rlc_spice}
\centering
\includegraphics[width=0.95\linewidth,height=4cm]{filtre.png}
\end{figure}
\begin{tabular}{c|c}
\end{tabular}

Les simulations sur \texttt{LTSpice} (voir schéma en figure~\ref{fig:rlc_spice} 
) avec la directive \texttt{transient} et en 
analyse \texttt{ac}, nous donnent les résultats suivants : 
$f_{0}=\SI{159.1}{\kilo\hertz}$, $G_{max}=\SI{-12.16}{\decibel}$, 
$f_{c1}=\SI{151.23}{\kilo\hertz}$ et $f_{c2}=\SI{167.39}{\kilo\hertz}$. On en 
déduit $Q=\num{159.1}/(\num{167.39}-\num{151.23}) \approx \num{9.85}$.

\begin{figure}[htbp]
\caption{Diagramme de Bode obtenu en simulation 
($G_\text{max}=\SI{-12.6}{\decibel}$, $f_{0}=\SI{159.1}{\kilo\hertz}$ et 
$\phi=\ang{-3.76}$).}
\label{fig:bode1}
\centering
\includegraphics[width=0.95\linewidth,height=6cm]{Bode FS.jpg}
\end{figure}

\subsection{Observations et conclusion}

On peut remarquer avec le diagramme de Bode obtenu en simulation (voir 
figure~\ref{fig:bode1}) que la largeur 
de la bande passante est bien de l'ordre de \SI{16}{\kilo\hertz} et celle-ci 
est centrée sur la fréquence d'intérêt de \SI{162}{\kilo\hertz}. Cependant, on 
trouve un gain maximal inférieur à 1 (gain théorique) et la fréquence de 
résonance n'est pas de \SI{162}{\kilo\hertz} mais de \SI{159.1}{\kilo\hertz}. 
La prochaine étape de notre montage sera l'amplification du signal obtenu après 
filtrage.

\section{Mesures (validation \emph{in situ})}

\subsection{Introduction}

\paragraph{Objectifs} Vérifier expérimentalement le dimensionnement du filtre 
sélectif et son adéquation au cahier des charges.

\paragraph{Hypothèses et modélisation}

\begin{itemize}
\item On devrait obtenir, d'après l'étude de dimensionnement, obtenir un filtre 
passe-bande centré sur $f_{0} = \SI{162}{\kilo\hertz}$.
\item D'après les simulations obtenues sur \texttt{LTSpice}, on s'attend 
également à trouver une allure similaire pour le diagramme de Bode et un 
gain maximum autour de \SI{-12.16}{\decibel}.
\item On devrait également trouver un grand facteur de qualité de l'ordre de 10.
\end{itemize}

\paragraph{Principe de manipulation}

Faire varier la fréquence du signal d'entrée et mesurer la réponse en 
fréquence, i.e.~gain et phase, du signal de sortie du filtre.

\subsection{Protocole}

\paragraph{Matériel et références}

\begin{itemize}
    \item Générateur 
    \item Oscilloscope numérique TEKTRONIX TBS 1052B-EDU
    \item Générateur personnel (GBF, DDS JDS6600)
    \item Oscilloscope numérique personnel (Hanmatek DOS1102)
\end{itemize}

\paragraph{Méthode de mesures} Le montage a été réalisé avec deux GBF et 
oscilloscopes différents et le protocole suivant.

\begin{itemize}
    \item Réaliser le montage du filtre proposé sur la plaque à trou.
    \item Régler le GBF pour délivrer un signal en entré avec une amplitude de 
    \SI{8.2}{\volt} et une fréquence de \SI{162}{\kilo\hertz}.
    \item Brancher sur la voie 1 de l'oscilloscope la tension d'entré du 
    générateur et sur la voie 2 la tension obtenue en sortie du filtre.
    \item Vérifier la cohérence entre l'atténuation de la sonde et celle donnée 
    par l'oscilloscope.
    \item Relever alors à l'aide des curseurs de l'oscilloscope (Bouton 
    \emph{Measures} puis sélectionner \emph{amplitude}), l'amplitude de chaque 
    signal.
    \item Consigner les résultats dans un tableur.
    \item Changer la fréquence pour une plage de $f \in 
    \{\SI{1}{\kilo\hertz};\SI{1}{\mega\hertz}\}$ et relever à nouveau les 
    amplitudes pour chacune des fréquences.
    \item Tracer sur papier logarithmique le diagramme de Bode obtenu.
    \item Passer en mode $XY$ sur l'oscilloscope et chercher la fréquence de 
    résonance du filtre, i.e.~lorsque l'on obtient une droite.
\end{itemize}

\subsection{Observations}

\paragraph{Mesures}

Nous avons réalisé une série de mesures et obtenu le diagramme de Bode 
représenté en figure~\ref{fig:bode2}, avec les mesures reportées dans la 
table~\ref{fig:tab}.

\begin{figure*}[tbp]
\caption{Diagramme de Bode en amplitude relevé expérimentalement.}
\label{fig:bode2}
\centering
\begin{tikzpicture}
\begin{semilogxaxis}[
width=0.95\linewidth,height=6cm,
  xlabel = {fréquence [\si{\kilo\hertz}]},
  ylabel = {Gain [\si{\decibel}]},xmin=1,xmax=1e3,
  ymin = -32, ymax = 0,grid=both,legend entries={relevé expérimental,
filtre souhaité (cahier des charges)},legend pos=north west]
  \addplot[blue,mark=o] table[x index=2,y index=3,col sep=comma] 
  {example_filtreRLC_table.csv};
  \addplot[red,domain=1:1e3,samples=100,smooth] 
  {-10*log10(1+100*(x/162-162/x)^2)};
\end{semilogxaxis}
\end{tikzpicture}
\end{figure*}

\begin{table}[htbp]
\centering
\caption{Mesures relevées pour le diagramme de Bode.}
\label{fig:tab}
    \pgfplotstabletypeset[
      multicolumn names, % allows to have multicolumn names
      col sep=comma, % the seperator in our .csv file
      display columns/0/.style={
		column name=$V_e$, % name of first column
		column type={S},string type},  % use siunitx for formatting
      display columns/1/.style={
		column name=$V_s$,
		column type={S},string type},
      display columns/2/.style={
		column name=$f$,
		column type={S},string type},
      display columns/3/.style={
		column name={$G=V_s/V_e$},
		column type={S},string type},
      every head row/.style={
		before row={\toprule}, % have a rule at top
		after row={
			{[\si{\volt}]} & {[\si{\volt}]} & {[\si{\kilo\hertz}]} & 
			{[\si{\decibel}]}\\ 
			% 
			%the units 
			%seperated by &
			\cmidrule(lr){1-1}\cmidrule(lr){2-2}\cmidrule(lr){3-3}\cmidrule(lr){4-4}}
			 % rule under units
			},
		every last row/.style={after row=\bottomrule}, % rule at bottom
    ]{example_filtreRLC_table.csv} % filename/path to file
\end{table}

\paragraph{Description des mesures}

Nous avons pu constater lors des mesures que le gain maximal obtenu ne 
correspondait pas à celui de la simulation. Ainsi, le gain maximal obtenu était 
de l'ordre de \SI{-2.7}{\decibel} pour une fréquence $f = 
\SI{162}{\kilo\hertz}$ au lieu de \SI{-12.16}{\decibel} en simulation. De plus, 
en passant au mode $XY$, on peut déterminer avec précision la fréquence de 
résonance du filtre, on trouve alors une fréquence de \SI{154.8(1)}{\kilo\hertz}
ce qui est cohérent avec le résultat de la simulation avec une fréquence de 
résonance de \SI{159.1}{\kilo\hertz}.

On a par ailleurs bien un filtre passe bande autour de \SI{162}{\kilo\hertz} et 
on obtient un diagramme de Bode similaire à celui obtenu en simulation. Enfin, 
on peut voir que le filtre est relativement sélectif.

\subsection{Conclusion}

\paragraph{Analyse des résultats}
Les mesures ont déjà permis de déterminer une fréquence centrale 
$f_0=\SI{154.8(1)}{\kilo\hertz}$ pour laquelle le 
gain est maximal, $\text{GdB}_\text{max}=\SI{-2.7}{\decibel}$.

En exploitant les autres mesures, on cherche les deux fréquences pour 
lesquelles le gain est égal à $\text{GdB}_\text{max}-\SI{3}{\decibel}$ 
(cf.~diagramme de Bode de la figure~\ref{fig:bode2}). On trouve 
$f_1=\SI{152(1)}{\kilo\hertz}$ et $f_1=\SI{168(1)}{\kilo\hertz}$.
Ainsi, l'on peut déterminer le facteur de qualité en calculant le rapport entre 
la fréquence centrale $f_0$ et l'écart des fréquences de coupure $f_2-f_1$, 
soit environ $Q=160/(168-152)\approx \num{10.0(1)}$.
On trouve donc bien un résultat cohérent avec la partie théorique où le facteur 
de qualité attendu était de l'ordre de $10$. 


Le montage réalisé assure bien la fonction souhaitée. On a bien un filtre dont 
la bande passante est centrée sur \SI{162}{\kilo\hertz}. Cependant, il existe 
quelques incohérences entre les simulations de \texttt{LTSpice} et les mesures 
expérimentales notamment pour la valeur du gain maximale obtenue. De plus, la 
fréquence centrale observée n'est pas tout à fait de \SI{162}{\kilo\hertz} 
puisque nous avons dimensionné les composants dans la série E12 dont la valeur 
n'est pas exactement celle déterminée à l'étape de dimensionnement. Cependant, 
nous pouvons finalement conclure que celui-ci reste cohérent et donne les 
résultats acceptables.

\paragraph{Ouverture}

Nous pouvons modifier le montage en plaçant deux condensateurs en parallèle 
afin d'obtenir une fréquence de résonance de \SI{162}{\kilo\hertz}.

\section*{Annexes}\addcontentsline{toc}{section}{Annexes}

Les figures \ref{fig:oscillo_162}, \ref{fig:oscillo_156}, 
\ref{fig:oscillo_XY_156} et \ref{fig:oscillo_XY_162} 
détaillent quelques résultats expérimentaux complémentaires.

\begin{figure}[htbp]
\caption{Oscillogramme obtenu pour $f=\SI{162}{\kilo\hertz}$.}
\label{fig:oscillo_162}
\centering
\includegraphics[width=.9\linewidth,height=5cm]{RLC162.png}
\end{figure}

\begin{figure}[htbp]
\caption{Oscillogramme obtenu pour $f=\SI{156}{\kilo\hertz}$.}
\label{fig:oscillo_156}
\centering
\includegraphics[width=.9\linewidth,height=5cm]{RLC 156.png}
\end{figure}

\begin{figure}[htbp]
\caption{Mode $XY$ obtenu pour $f=\SI{156}{\kilo\hertz}$.}
\label{fig:oscillo_XY_156}
\centering
\includegraphics[width=.9\linewidth,height=5cm]{XY 157.png}
\end{figure}


\begin{figure}[htbp]
\caption{Mode $XY$ obtenu pour $f=\SI{162}{\kilo\hertz}$.}
\label{fig:oscillo_XY_162}
\centering
\includegraphics[width=.9\linewidth,height=5cm]{XY 162.png}
\end{figure}

\end{document}
